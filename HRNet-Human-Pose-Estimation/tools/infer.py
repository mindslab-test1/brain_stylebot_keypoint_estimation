from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import os
import pprint
import time
import math

import torch
import torch.nn.parallel
import torch.optim
import torch.utils.data
import torch.utils.data.distributed
import torchvision.transforms as transforms
import torchvision.utils as utils

import numpy as np
import cv2
import PIL.Image as Image
import PIL.ImageOps as ImageOps
import pickle as pkl
from tqdm import tqdm

import _init_paths
from core.inference import get_max_preds
from config import cfg
from config import update_config
from models.pose_hrnet import get_pose_net
from utils.values import KP_MAP_AGGREGATE, CATEGORIES, KP_REGION_DICT
# from utils.vis import save_debug_images

import dataset
import models

RESIZE_H = 512
RESIZE_W = 384

def parse_args():
    parser = argparse.ArgumentParser(description='Infer keypoints network')
    # general
    parser.add_argument('--cfg',
                        help='experiment configure file name',
                        required=True,
                        type=str)

    parser.add_argument('opts',
                        help="Modify config options using the command-line",
                        default=None,
                        nargs=argparse.REMAINDER)

    parser.add_argument('--modelDir',
                        help='model directory',
                        type=str,
                        default='')
    parser.add_argument('--logDir',
                        help='log directory',
                        type=str,
                        default='')
    parser.add_argument('--dataDir',
                        help='data directory',
                        type=str,
                        default='')
    parser.add_argument('--prevModelDir',
                        help='prev Model directory',
                        type=str,
                        default='')

    parser.add_argument('--img_dir',
                        help='image directory',
                        type=str,
                        default='')

    parser.add_argument('--class_id',
                        help='clothes category',
                        type=int,
                        default=-1)

    parser.add_argument('--save_img', action='store_true')
    parser.add_argument('--save_keypoint', action='store_true')
    parser.add_argument('--use_width_pad', action='store_true')

    args = parser.parse_args()
    return args


def get_keypoints(image, batch_joints, class_id, margin, width_pad=0):
    ndarr = np.array(image)
    scale_x = (ndarr.shape[1] + 2 * margin['w_margin']) / 96
    scale_y = (ndarr.shape[0] + 2 * margin['h_margin']) / 128

    cat_idx = [x - 1 for x in KP_MAP_AGGREGATE[class_id]]
    joints = batch_joints[0][cat_idx, :]  # 81 x 2

    if width_pad:
        margin['w_margin'] += width_pad

    joints[:, 0] = (joints[:, 0] * scale_x) - margin['w_margin']
    joints[:, 1] = (joints[:, 1] * scale_y) - margin['h_margin']
    joints.astype(np.int32)
    return joints


# originally from utils.vis
def save_image_with_keypoints(image, joints, file_name, width_pad=0):
    ndarr = np.array(image)
    joints_vis = [1 for _ in range(joints.shape[0])]  # not used now

    if width_pad:
        ndarr = ndarr[:, width_pad:width_pad * 11, :]

    for joint, joint_vis in zip(joints, joints_vis):
        cv2.circle(ndarr, (joint[0], joint[1]), 2, [255, 0, 0], 2)

    cv2.imwrite(file_name, ndarr[:, :, ::-1])


def save_keypoints(joints, file_name):
    with open(file_name, 'wb') as f:
        pkl.dump(joints, f)


def add_width_pad(pil_image):
    h, w = pil_image.height, pil_image.width
    width_pad = w // 10
    return ImageOps.expand(pil_image, (width_pad, 0)), width_pad


def pad_image(pil_image):

    h, w = pil_image.height, pil_image.width
    if h / w > RESIZE_H / RESIZE_W:  # if h is too large
        new_w = int(h * RESIZE_W / RESIZE_H)
        new_image = ImageOps.expand(pil_image, ((new_w - w) // 2, 0))
        return new_image, {'w_margin': (new_w - w) // 2, 'h_margin': 0, 'scale': h / RESIZE_H}
    else:
        new_h = int(w * RESIZE_H / RESIZE_W)
        new_image = ImageOps.expand(pil_image, (0, (new_h - h) // 2))
        return new_image, {'w_margin': 0, 'h_margin': (new_h - h) // 2, 'scale': w / RESIZE_W}


def main():
    args = parse_args()
    update_config(cfg, args)

    model = get_pose_net(cfg, is_train=False)

    model_state_file = os.path.join('pretrained', 'final_state.pth')
    model.load_state_dict(torch.load(model_state_file))

    model = torch.nn.DataParallel(model, device_ids=cfg.GPUS).cuda()

    categories = len(KP_MAP_AGGREGATE)
    regions = len(KP_REGION_DICT)

    # Data loading code
    transform_tensor = transforms.Compose([
        transforms.Resize((RESIZE_H, RESIZE_W), interpolation=Image.BICUBIC),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
        ])

    image_files = sorted([os.path.join(args.img_dir, im_file) for im_file in os.listdir(args.img_dir)
                   if (im_file.lower().endswith('.png') or im_file.lower().endswith('.jpeg') or
                       im_file.lower().endswith('.jpg')) and 'converted' not in im_file])

    im_list = [Image.open(im_file).convert('RGB') for im_file in image_files]
    # im_list = [cv2.imread(im_file, cv2.IMREAD_COLOR | cv2.IMREAD_IGNORE_ORIENTATION) for im_file in image_files]
    # im_list = [cv2.cvtColor(im_array, cv2.COLOR_BGR2RGB) for im_array in im_list]
    im_tensor = [transform_tensor(im_array).unsqueeze(0) for im_array in im_list]

    if args.class_id == -1:
        raise AssertionError("class_id should be from 0 to 12")

    # switch to evaluate mode
    model.eval()
    with torch.no_grad():
        # time start
        end = time.time()
        for i, im_elem in tqdm(enumerate(im_list)):

            if args.use_width_pad:
                im_elem, width_pad = add_width_pad(im_elem)
            else:
                width_pad = 0

            im_new, scale_result = pad_image(im_elem)
            im_input = transform_tensor(im_new).unsqueeze(0)
            # compute output
            outputs = model(im_input)
            if isinstance(outputs, list):
                output = outputs[-1]
            else:
                output = outputs

            heatmap = output.detach().cpu().numpy()
            preds, maxvals = get_max_preds(heatmap)
            maxvals = np.squeeze(maxvals)

            cat_score = np.zeros(categories)
            region_score = np.zeros(regions)
            region_list = list(KP_REGION_DICT.values())
            for cat in range(categories):
                cat_idx = [x - 1 for x in KP_MAP_AGGREGATE[cat]]
                cat_score[cat] = np.median(maxvals[cat_idx])
            for region in range(regions):
                region_idx = [x - 1 for x in region_list[region]]
                region_score[region] = np.median(maxvals[region_idx])

            # print(os.path.splitext(image_files[i])[0])

            # TODO: classify which category it is in with maxvals
            # for key, score in zip(KP_REGION_DICT.keys(), region_score):
                # print(key, "{:.2f}".format(score))

            joints = get_keypoints(im_elem, preds, args.class_id, scale_result, width_pad=width_pad)

            if args.save_img:
                save_image_with_keypoints(im_elem, joints, os.path.splitext(image_files[i])[0] + "_converted.jpg",
                                          width_pad=width_pad)

            if args.save_keypoint:
                save_keypoints(joints, os.path.splitext(image_files[i])[0] + ".pkl")

    # time end
    print((time.time() - end) / len(im_tensor))


if __name__ == '__main__':
    main()
