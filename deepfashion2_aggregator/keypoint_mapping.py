import numpy as np

KP_MAP_AGGREGATE = [
    [1, 2, 3, 4, 5, 6, 9, 10, 11, 12, 13, 14, 41, 42, 43, 47, 44, 45, 46, 15,
     16, 17, 18, 19, 20],
    [1, 2, 3, 4, 5, 6, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 41, 42, 43, 47,
     44, 45, 46, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40],
    [1, 2, 3, 7, 5, 6, 9, 10, 11, 12, 13, 14, 41, 42, 43, 53, 44, 45, 46, 15,
     16, 17, 18, 19, 20, 8, 48, 49, 50, 51, 52],
    [1, 2, 3, 7, 5, 6, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 41, 42, 43, 53,
     44, 45, 46, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 8, 48, 49, 50, 51, 52],
    [1, 2, 3, 4, 5, 6, 54, 41, 42, 43, 47, 44, 45, 46, 55],
    [1, 2, 3, 4, 5, 6, 56, 41, 42, 43, 47, 44, 45, 46, 57],
    [63, 64, 65, 71, 66, 67, 70, 68, 69, 72],
    [63, 64, 65, 71, 73, 66, 67, 74, 70, 75, 68, 69, 76, 72],
    [63, 64, 65, 77, 78, 79, 80, 81],
    [1, 2, 3, 4, 5, 6, 9, 10, 11, 12, 13, 14, 41, 42, 43, 58, 59, 60, 61, 62,
     44, 45, 46, 15, 16, 17, 18, 19, 20],
    [1, 2, 3, 4, 5, 6, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 41, 42, 43, 58,
     59, 60, 61, 62, 44, 45, 46, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40],
    [1, 2, 3, 4, 5, 6, 54, 41, 42, 43, 58, 59, 60, 61, 62, 44, 45, 46, 55],
    [1, 2, 3, 4, 5, 6, 56, 41, 42, 43, 58, 59, 60, 61, 62, 44, 45, 46, 57]
]

KP_MAP = [
    list(range(0, 25)),
    list(range(25, 58)),
    list(range(58, 89)),
    list(range(89, 128)),
    list(range(128, 143)),
    list(range(143, 158)),
    list(range(158, 168)),
    list(range(168, 182)),
    list(range(182, 190)),
    list(range(190, 219)),
    list(range(219, 256)),
    list(range(256, 275)),
    list(range(275, 294)),
]



def keypoint_map(cat, landmarks, aggregate=True):
    if aggregate:
        points = np.zeros(81 * 3)
    else:
        points = np.zeros(294 * 3)

    points_x = landmarks[0::3]
    points_y = landmarks[1::3]
    points_v = landmarks[2::3]
    points_x = np.array(points_x)
    points_y = np.array(points_y)
    points_v = np.array(points_v)

    if aggregate:
        for n in range(len(points_x)):
            mapped_n = KP_MAP_AGGREGATE[cat - 1][n] - 1  # mapping index starts with 1
            points[3 * mapped_n] = points_x[n]
            points[3 * mapped_n + 1] = points_y[n]
            points[3 * mapped_n + 2] = points_v[n]
    else:
        for n in range(len(points_x)):
            mapped_n = KP_MAP[cat - 1][n]
            points[3 * mapped_n] = points_x[n]
            points[3 * mapped_n + 1] = points_y[n]
            points[3 * mapped_n + 2] = points_v[n]

    num_points = len(np.where(points_v > 0)[0])

    return points, num_points