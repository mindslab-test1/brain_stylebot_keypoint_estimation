def make_base_dict(aggregate=False):
    if aggregate:
        keypoints = list(map(str, list(range(1, 82))))
    else:
        keypoints = list(map(str, list(range(1, 295))))

    dataset = {
        "info": {},
        "licenses": [],
        "images": [],
        "annotations": []
    }

    dataset['categories'] = [
        {
            'id': 1,
            'name': "short_sleeved_shirt",
            'supercategory': "clothes",
            'keypoints': keypoints,
            'skeleton': []
        },
        {
            'id': 2,
            'name': "long_sleeved_shirt",
            'supercategory': "clothes",
            'keypoints': keypoints,
            'skeleton': []
        },
        {
            'id': 3,
            'name': "short_sleeved_outwear",
            'supercategory': "clothes",
            'keypoints': keypoints,
            'skeleton': []
        },
        {
            'id': 4,
            'name': "long_sleeved_outwear",
            'supercategory': "clothes",
            'keypoints': keypoints,
            'skeleton': []
        },
        {
            'id': 5,
            'name': "vest",
            'supercategory': "clothes",
            'keypoints': keypoints,
            'skeleton': []
        },
        {
            'id': 6,
            'name': "sling",
            'supercategory': "clothes",
            'keypoints': keypoints,
            'skeleton': []
        },
        {
            'id': 7,
            'name': "shorts",
            'supercategory': "clothes",
            'keypoints': keypoints,
            'skeleton': []
        },
        {
            'id': 8,
            'name': "trousers",
            'supercategory': "clothes",
            'keypoints': keypoints,
            'skeleton': []
        },
        {
            'id': 9,
            'name': "skirt",
            'supercategory': "clothes",
            'keypoints': keypoints,
            'skeleton': []
        },
        {
            'id': 10,
            'name': "short_sleeved_dress",
            'supercategory': "clothes",
            'keypoints': keypoints,
            'skeleton': []
        },
        {
            'id': 11,
            'name': "long_sleeved_dress",
            'supercategory': "clothes",
            'keypoints': keypoints,
            'skeleton': []
        },
        {
            'id': 12,
            'name': "vest_dress",
            'supercategory': "clothes",
            'keypoints': keypoints,
            'skeleton': []
        },
        {
            'id': 13,
            'name': "sling_dress",
            'supercategory': "clothes",
            'keypoints': keypoints,
            'skeleton': []
        }]

    return dataset
