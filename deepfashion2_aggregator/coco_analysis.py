import os
import json
from pycocotools.coco import COCO


if __name__ == '__main__':
    path = os.path.join('validation', 'deepfashion2_validation_aggregated_coco.json')
    coco = COCO(path)
    image_ids = coco.getImgIds()

    print(len(image_ids))

    obj_cnt = 0
    obj_kp_cnt = 0
    for index in image_ids[:1]:
        im_ann = coco.loadImgs(index)[0]

        annIds = coco.getAnnIds(imgIds=index, iscrowd=False)
        objs = coco.loadAnns(annIds)

        for idx, obj in enumerate(objs):
            for key, value in obj.items():
                print(key, value)
