from PIL import Image
import numpy as np
from tqdm import tqdm

import os, json, argparse

from coco_dict import make_base_dict
from keypoint_mapping import keypoint_map

def make_coco_dict(mode, aggregate):
    # add category ids
    dataset = make_base_dict(aggregate=aggregate)

    # directory path
    json_dir = os.path.join(mode, 'annos')
    image_dir = os.path.join(mode, 'image')
    num_images = len(os.listdir(image_dir))

    sub_index = 0  # the index of ground truth instance
    # for each json file and its corresponding image,
    for num in tqdm(range(1, num_images + 1)):
        json_name = os.path.join(json_dir, str(num).zfill(6) + '.json')
        image_name = os.path.join(image_dir, str(num).zfill(6) + '.jpg')

        if (num >= 0):
            imag = Image.open(image_name)
            width, height = imag.size
            with open(json_name, 'r') as f:
                temp = json.loads(f.read())
                pair_id = temp['pair_id']

                # set an image header
                dataset['images'].append({
                    'coco_url': '',
                    'date_captured': '',
                    'file_name': str(num).zfill(6) + '.jpg',
                    'flickr_url': '',
                    'id': num,
                    'license': 0,
                    'width': width,
                    'height': height
                })
                for i in temp:
                    if i == 'source' or i == 'pair_id':
                        continue
                    else:
                        # designate the number
                        sub_index = sub_index + 1

                        # set bounding box
                        box = temp[i]['bounding_box']
                        w = box[2] - box[0]
                        h = box[3] - box[1]
                        x_1 = box[0]
                        y_1 = box[1]
                        bbox = [x_1, y_1, w, h]

                        # set category id (following deepfashion2)
                        cat = temp[i]['category_id']
                        # set style
                        style = temp[i]['style']
                        seg = temp[i]['segmentation']
                        landmarks = temp[i]['landmarks']

                        # map landmarks with mapping fuction
                        points, num_points = keypoint_map(cat, landmarks, aggregate=aggregate)

                        # append in annotations
                        dataset['annotations'].append({
                            'area': w * h,
                            'bbox': bbox,
                            'category_id': cat,
                            'id': sub_index,
                            'pair_id': pair_id,
                            'image_id': num,
                            'iscrowd': 0,
                            'style': style,
                            'num_keypoints': num_points,
                            'keypoints': points.tolist(),
                            'segmentation': seg,
                        })

    return dataset


if __name__ == '__main__':
    mode = 'validation'
    aggregate = True

    dataset = make_coco_dict(mode, aggregate)

    if aggregate:
        json_name = os.path.join(mode, 'deepfashion2_' + mode + '_aggregated_coco.json')
    else:
        json_name = os.path.join(mode, 'deepfashion2_' + mode + '_coco.json')

    with open(json_name, 'w') as f:
        json.dump(dataset, f)
    f.close()
