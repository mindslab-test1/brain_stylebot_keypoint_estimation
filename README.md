## Prerequisite

### Install

* install HRNet dependencies as in [HRNet/HRNet-Human-Pose-Estimation](https://github.com/HRNet/HRNet-Human-Pose-Estimation), then

    ```
    cd HRNet-Human-Pose-Estimation/lib
    make
    ```
  
### Pretrained

* pre-trained weight를 [다운로드](https://drive.google.com/file/d/1WqtRbISjaJlTDUkZxI7T1Wgb7qioZOKQ/view?usp=sharing) 받아서 **HRNet-Human-Pose-Estimation/pretrained/fianl_state.pth** 에 넣는다.



## Usage

### Training

* Aggregation

    ```bash
    python tools/train.py --cfg experiments/deepfashion2/w48_512x384_adam_lr1e-3-agg81kps.yaml
    ```

* Finetune

    ```bash
    # finetuning 1st category
    python tools/train.py --cfg experiments/deepfashion2/w48_512x384_adam_lr1e-3-agg81kps-category1-hflip.yaml
    # finetuning 2nd category
    python tools/train.py --cfg experiments/deepfashion2/w48_512x384_adam_lr1e-3-agg81kps-category2-hflip.yaml
    # finetuning others ...
    ```


​    

### Inference

* Class ID는  
short sleeve top: 0, long sleeve top: 1, short sleeve outwear: 2, long sleeve outwear: 3, vest: 4, sling:5  
shorts: 6, trousers: 7, skirt: 8, short sleeve dress: 9, long sleeve dress: 10  
vest dress: 11, sling dress: 12


* Class ID를 미리 알고 있다는 전제 하에 진행된다.

    ```bash
    cd HRNet-Human-Pose-Estimation
    python tools/infer.py --cfg experiments/deepfashion2/w48_512x384_adam_lr1e-3-agg81kps.yaml --img_dir /path/to/infer_image_dir --class_id (class id) --save_keypoint
    ```



